module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            }
        },

        cssmin: {
            css:{
                src: 'css/style.dev.css',
                dest: 'css/style.css'
            }
        },

        watch: {
            css: {
                files: ['css/style.dev.css'],
                tasks: ['cssmin:css']
            }
        },

        requirejs: {
            compile: {
                options: {
                    name: 'app',
                    baseUrl: "js/",
                    mainConfigFile: "js/app.js",
                    out: "js/app.js",
                    preserveLicenseComments: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-requirejs');

    grunt.registerTask('prod', [
        'cssmin:css',
        'requirejs'
    ]);
    grunt.registerTask('dev', [
        'watch:css'
    ]);
    grunt.registerTask('default', ['uglify']);
};