require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        handlebars: {
            exports: 'Handlebars'
        }
    },
    paths: {
        "jquery": "../bower_components/jquery/dist/jquery.min",
        "underscore": "../bower_components/underscore/underscore-min",
        "backbone": "../bower_components/backbone/backbone",
        "text": "../bower_components/requirejs-text/text",
        "handlebars": "../bower_components/handlebars/handlebars",
        "raphael": "../bower_components/raphael/raphael-min",
        "backbone.raphael": "../bower_components/backbone.raphael-amd/backbone.raphael"
    }
});

require(['jquery', 'backbone', 'app/router'], function ($, Backbone, Router) {
    var router = new Router();
    Backbone.history.start();
});