define([
        'jquery',
        'handlebars',
        'backbone',
        'text!../../tpl/slider.html',
        'raphael',
        '../models/slider',
        'backbone.raphael',
        'text!../../tpl/item.html',
        'text!../../tpl/itemFirst.html',
    ],
    function ($, Handlebars, Backbone, sliderTemplate, Raphael, Slider, BackboneRaphael, SliderItemTemplate, SliderItemFirstTemplate) {
        this.sliderArray = Slider;
        this.sliderItemTemplate = SliderItemTemplate;
        this.sliderItemFirstTemplate = SliderItemFirstTemplate;
        this.Handlebars = Handlebars;
        this.firstNavIndex = this.sliderArray.Slides.length - 2;
        this.lastNavIndex = 2;
        this.currentIndex = 0;

        var HomeView = Backbone.View.extend({
            el: '#content',
            initialize: function(options) {
                _.bindAll(this, 'beforeRender', 'render', 'afterRender');
                var _this = this;
                this.render = _.wrap(this.render, function(render) {
                    _this.beforeRender();
                    render();
                    _this.afterRender();
                    return _this;
                });
            },

            beforeRender: function() {

            },
            render: function () {

                Handlebars.registerHelper('listItem', function (from, to, context, options){
                    var item = "";
                    for (var i = from, j = to; i < j; i++) {
                        item = item + options.fn(context[i]);
                    }
                    return item;
                });

                var template = Handlebars.compile(sliderTemplate);

                this.$el.html(template({
                    initSlider: Slider.InitArray,
                    slides: Slider.Slides
                }));
            },
            afterRender: function() {
                var paperNext = Raphael($('#controlNext').get(0), 30, 30);
                var RArrowView = BackboneRaphael.RaphaelView.extend({
                    render: function(){
                        var rect = paperNext.path(Slider.RArrowPath);
                        rect.attr({'fill': Slider.ArrowColor});

                    }
                });

                var RArrow = new RArrowView({ paper: paperNext });
                RArrow.render();

                var paperPrev = Raphael($('#controlPrev').get(0), 30, 30);
                var LArrowView = Backbone.RaphaelView.extend({
                    render: function(){
                        var rect = paperPrev.path(Slider.LArrowPath);
                        rect.attr('fill', Slider.ArrowColor);
                    }
                });
                var LArrow = new LArrowView({ paper: paperPrev });
                LArrow.render();

                changeSizeContainer($('#slider .images li'));
                resizeContainer($('#slider .images li'));
                setTotalWidth($('#slider .images li'));
                initNextNavigation($('#slider .images li'));
                initPrevNavigation($('#slider .images li'));
                initTriggers();
                $(window).resize(function(){
                    changeSizeContainer($('#slider .images li'));
                    resizeContainer($('#slider .images li'));
                    setTotalWidth($('#slider .images li'));
                    initNextNavigation($('#slider .images li'));
                    initPrevNavigation($('#slider .images li'));
                });
            }
        });

        return new HomeView();
    }),
    changeSizeContainer = function(container) {
        var newWidth = $(window).width() * 0.8;
        var newHeight = newWidth/1.5;

        container.height(newHeight);
        container.parents('#slider').height(newHeight +30);
        container.width(newWidth);
    },
    changeWidthImg = function(img, container) {
        var width = img.width();

        if (width > container.width()) {
            var diff = width - container.width();
            var newDiff = diff/2;

            img.css({
                'margin-left': '-' + newDiff + 'px'
            });
        } else {
            img.width('100%');
        }
    },
    resizeContainer = function(container) {
        var img = container.find('img');

        img.each(function(index, element) {
            var img = $(element);
            var imgHeight = img.height();
            var containerHeight = container.height();

            if (imgHeight < containerHeight) {
                img.height('100%');
                changeWidthImg(img, container);
            } else if (imgHeight > containerHeight) {
                var diff = imgHeight - containerHeight;
                var newDiff = diff/2;

                img.css({
                    'margin-top': '-' + newDiff + 'px'
                });

                changeWidthImg(img, container);
            }

        });
    },
    setTotalWidth = function(container) {
        var parentContainer = container.parent('.images');
        var total = 0;
        var marginLeft = 0;

        if (container.length >= 5) {
            var containerWidth = container.width();
            total = containerWidth * 5;
            marginLeft = containerWidth * 2;
        } else {
            container.each(function(index, element) {
                total += $(element).width();
            });
        }

        parentContainer.width(total);
        parentContainer.css({
            'margin-left': '-' + marginLeft + 'px'
        });

    },
    initTriggers = function () {
        var length = this.sliderArray.Slides.length;
        if(this.currentIndex > length-1) {
            this.currentIndex = 0;
        }

        if (this.currentIndex < 0) {
            this.currentIndex = length + this.currentIndex;
        }

        $('.triggers').find('li').removeClass('current');
        $('.triggers').find('li#trigger' + this.currentIndex).addClass('current');
    },
    initNextNavigation = function(container) {
        var width = container.width();
        var parentContainer = container.parent('.images');
        var margin = parentContainer.css('margin-left');
        var self = this;

        $('#controlNext').click(function(){
            var curBtn = $(this);
            $(this).attr('disabled', 'disabled');
            $('.images li').first().animate({
                width: 0
            }, 2000, function() {
                $('.images li').first().remove();

                var sliderLength = self.sliderArray.Slides.length;
                var slideIndex = ++self.lastNavIndex;
                if (slideIndex > sliderLength - 1) {
                    self.lastNavIndex = 0;
                }
                var nextSlide = self.sliderArray.Slides[self.lastNavIndex];

                self.firstNavIndex = self.lastNavIndex - 4;
                if (self.firstNavIndex < 0) {
                    self.firstNavIndex += sliderLength;
                }

                var template = self.Handlebars.compile(self.sliderItemTemplate);

                $('.images').append(template({
                    path: nextSlide.path,
                    name: nextSlide.name
                }));

                ++self.currentIndex;
                initTriggers();
                changeSizeContainer($('#slider .images li'));
                resizeContainer($('#slider .images li'));
                curBtn.removeAttr('disabled')
            });
        });
    },
    initPrevNavigation = function(container) {
        var width = container.width();
        var parentContainer = container.parent('.images');
        var margin = parentContainer.css('margin-left');
        var finalMargin = parseInt(margin) + width;
        var self = this;
        var currentIndex = 0;

        $('#controlPrev').click(function(){
            var sliderLength = self.sliderArray.Slides.length;
            $(this).attr('disabled','disabled');
            $('.images li').last().remove();
            --self.firstNavIndex;
            if (self.firstNavIndex < 0) {
                self.firstNavIndex = sliderLength-1;
            }
            self.lastNavIndex = self.firstNavIndex + 4;

            if (self.lastNavIndex > sliderLength-1) {
                self.lastNavIndex -= sliderLength;
            }

            var nextSlide = self.sliderArray.Slides[self.firstNavIndex];
            var template = self.Handlebars.compile(self.sliderItemFirstTemplate);

            $('.images').prepend(template({
                path: nextSlide.path,
                name: nextSlide.name
            }));

            $('.images li').first().animate({
                width: '+=' + width
            }, 2000, function(){
                $(this).removeAttr('disabled');
                --self.currentIndex;
                initTriggers();
                changeSizeContainer($('#slider .images li'));
            });
        });
    }
;