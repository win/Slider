define([
        'backbone',
        'app/views/slider'
    ],
    function (Backbone, sliderView) {

    return Backbone.Router.extend({

        routes: {
            '': 'slider'
        },

        slider: function () {
            sliderView.render();
        }

    });

});