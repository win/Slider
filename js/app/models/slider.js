define([
        'jquery',
        'backbone'
    ],
    function ($, Backbone) {
        var  slides = [
                {"id":0, "path": "../../images/slider/img0.jpg", name: "name0", class: "current"},
                {"id":1, "path": "../../images/slider/img1.jpg", name: "name1", class: ""},
                {"id":2, "path": "../../images/slider/img2.jpg", name: "name2", class: ""},
                {"id":3, "path": "../../images/slider/img3.jpg", name: "name3", class: ""},
                {"id":4, "path": "../../images/slider/img4.jpg", name: "name4", class: ""},
                {"id":5, "path": "../../images/slider/img5.jpg", name: "name5", class: ""},
                {"id":6, "path": "../../images/slider/img6.jpg", name: "name6", class: ""},
                {"id":7, "path": "../../images/slider/img7.jpg", name: "name7", class: ""},
                {"id":8, "path": "../../images/slider/img8.jpg", name: "name8", class: ""},
                {"id":9, "path": "../../images/slider/img9.jpg", name: "name9", class: ""},
                {"id":10, "path": "../../images/slider/img10.jpg", name: "name10", class: ""},
                {"id":11, "path": "../../images/slider/img11.jpg", name: "name11", class: ""},
                {"id":12, "path": "../../images/slider/img12.jpg", name: "name12", class: ""},
                {"id":13, "path": "../../images/slider/img13.jpg", name: "name13", class: ""}
            ],

        initArray = slides.slice(-2).concat(slides.slice(0,3)),

        ArrowColor = '#000',

        LArrowPath = 'M21.871,9.814 15.684,16.001 21.871,22.188 18.335,25.725 8.612,16.001 18.335,6.276z',

        RArrowPath = '1M10.129,22.186 16.316,15.999 10.129,9.812 13.665,6.276 23.389,15.999 13.665,25.725z',

        findById = function (id) {
            var deferred = $.Deferred(),
                slide = null,
                l = slides.length,
                i;
            for (i = 0; i < l; i = i + 1) {
                if (slides[i].id === id) {
                    slide = slides[i];
                    break;
                }
            }
            deferred.resolve(slide);
            return deferred.promise();
        },

        findByName = function (searchKey) {
            var deferred = $.Deferred(),
                results = slides.filter(function (element) {
                    var path = element.path;
                    return path;
                });
            deferred.resolve(results);
            return deferred.promise();
        },

        Slide = Backbone.Model.extend({
            initialize: function () {},
            sync: function (method, model, options) {
                if (method === "read") {
                    findById(parseInt(this.id)).done(function (data) {
                        options.success(data);
                    });
                }
            }
        }),

        SlideCollection = Backbone.Collection.extend({
            model: Slide,
            sync: function (method, model, options) {
                if (method === "read") {
                    findByName(options.data.name).done(function (data) {
                        options.success(data);
                    });
                }
            }
        });

    return {
        Slide: Slide,
        Slides: slides,
        InitArray: initArray,
        ArrowColor: ArrowColor,
        LArrowPath: LArrowPath,
        RArrowPath: RArrowPath,
        SlideCollection: SlideCollection
    };

});